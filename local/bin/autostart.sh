#!/bin/sh

function run {
 if ! pgrep $1 ;
  then
    $@&
  fi
}

run "lxsession"
run "udiskie"

run "nm-applet" 
run "volumeicon" 
run "mate-power-manager" 

run "dunst"
run "picom"
run "nitrogen --restore"
