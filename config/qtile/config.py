import os
import subprocess
from libqtile import hook

from modules.ezkeys import *
from modules.mouse import *
from modules.groups import *
from modules.scratchpads import *
from modules.screens import *
from modules.layouts import *


dgroups_key_binder = None
dgroups_app_rules = [] 
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = False
auto_minimize = True

wmname = "Qtile"

@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~/.local/bin/autostart.sh')
    subprocess.call([home])

# @hook.subscribe.setgroup
# def setgroup():
#     for i in qtile.groups:
#         if i is qtile.current_group:
#             i.set_label("󰁲")
#         else:
#             if i.windows:
#                 i.set_label("")
#             else:
#                 i.set_label("")
