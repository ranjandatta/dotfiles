import os
from dataclasses import dataclass

@dataclass(frozen=True)
class Preferences:
    terminal            =   "alacritty"
    browser             =   "firefox"
    private             =   "firefox --private-window"
    file_manager        =   "pcmanfm"
    screenshot_tool     =   'flameshot gui'
    code_editor         =   "vscodium"
    launcher            =   "rofi -show drun"
    power_menu          =   os.path.expanduser('~/.local/bin/powermenu.sh')

prefs = Preferences()
