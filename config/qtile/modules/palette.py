from dataclasses import dataclass

@dataclass(frozen=True)
class Gruvbox:
    transparent=  '#00000000'
    background=   '#282828'
    foreground=   '#d4be98'
    white=        '#f2e5bc'
    black=        '#3c3836'
    red=          '#ea6962'
    green=        '#a9b665'
    yellow=       '#d8a657'
    blue=         '#7daea3'
    magenta=      "#d3869b"
    cyan=         '#89b482'
    gray=         '#45403d'
    graylight=    '#654735'

class OneDark:
    transparent=  '#00000000'
    background=   '#1e2127'
    foreground=   '#abb2bf'
    white=        '#e6efff'
    black=        '#282c34'
    red=          '#e06c75'
    green=        '#98c379'
    yellow=       '#d19a66'
    blue=         '#61afef'
    magenta=      "#c678dd"
    cyan=         '#56b6c2'
    gray=         '#5c6370'
    graylight=    '#828791'

class TokyoNight:
    transparent=  '#00000000'
    background=   '#24283b'
    foreground=   '#a9b1d6'
    white=        '#acb0d0'
    black=        '#32344a'
    red=          '#f7768e'
    green=        '#9ece6a'
    yellow=       '#e0af68'
    blue=         '#7aa2f7'
    magenta=      "#ad8ee6"
    cyan=         '#449dab'
    gray=         '#444b6a'
    graylight=    '#9699a8'

class Nymph:
    transparent=  '#00000000'
    background=   '#1A2023'
    foreground=   '#BCC4C9'
    white=        '#BCC4C9'
    black=        '#252B2E'
    red=          '#BC7171'
    green=        '#9FBC85'
    yellow=       '#D5BE82'
    blue=         '#7C9BB4'
    magenta=      "#A883A2"
    cyan=         '#89B7B0'
    gray=         '#373D40'
    graylight=    '#5B6265'

palette = Nymph()
