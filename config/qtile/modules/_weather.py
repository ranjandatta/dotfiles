from qtile_extras import widget

class Weather(widget.OpenWeather):
    def __init__(self, **config):
        super().__init__(**config)
        self.toggled = False
        self.local = self.cityid
        self.add_callbacks({"Button1": self.toggle})

    def toggle(self):
        if self.toggled:
            self.cityid = self.local
        else:
            self.cityid = self.other_city
        self.toggled = not self.toggled
        self.update(self.poll())
        