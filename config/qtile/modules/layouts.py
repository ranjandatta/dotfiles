from libqtile import layout
from libqtile.config import Match

from modules.palette import palette

def init_layout_theme():
    return {
        "border_width"      :   12,
        "margin"            :   10,
        "border_focus"      :   [palette.background, palette.background,palette.background,palette.background,palette.background,palette.graylight],
        "border_normal"     :   [palette.background, palette.background,palette.background,palette.background,palette.background,palette.gray],
        "grow_amount"       :   5,
    }

def init_treetab_theme():
    return {
        'fontsize'          :   12,
        'section_fontsize'  :   18,
        'vspace'            :   5,
        'padding_y'         :   4,
        'padding_x'         :   2,
        'padding_left'      :   10,
        'margin_y'          :   10,
        'border_width'      :   2,
        'panel_width'       :   250,
        'section_top'       :   10,
        'section_left'      :   10,
        'section_bottom'    :   10,
        'section_padding'   :   10,
        'level_shift'       :   10,
        'previous_on_rm'    :   True,
        'place_right'       :   True,
        'bg_color'          :   palette.background,
        'active_bg'         :   palette.background,
        'inactive_bg'       :   palette.background,
        'active_fg'         :   palette.magenta,
        'inactive_fg'       :   palette.yellow,
        'section_fg'        :   palette.blue,
        'urgent_fg'         :   palette.background,
        'urgent_bg'         :   palette.red,
        'sections'          :   ['क्यों टाइल:'],
        'font'              :   'Roboto'
    }

def init_float_theme():
    return {
        'float_rules'       :   [
                                    *layout.Floating.default_float_rules,
                                    Match(wm_class="Pavucontrol"),  
                                    Match(wm_class="Nitrogen"),
                                    Match(wm_class="Lxappearance"),
                                ], 
        **init_layout_theme()
    }

layouts = [
    layout.MonadThreeCol(**init_layout_theme(), new_client_position='bottom', main_centered=False),
    layout.TreeTab(**init_treetab_theme()),
    layout.Floating(**init_layout_theme()),
]

floating_layout = layout.Floating(**init_float_theme())

