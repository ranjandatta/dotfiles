import os
from libqtile.config import Screen
from libqtile import bar, qtile

from modules.widgets import widgets_list
from modules.palette import palette

from modules._clock import Clock
from modules._weather import Weather

widget_defaults = dict(
    font="Roboto",
    fontsize = 16,
    padding = 12,
    foreground = palette.foreground,
)

extension_defaults = widget_defaults.copy()

bar_defaults = {
    'size': 28, 
    'border_width': 4, 
    'background': palette.background,
    'border_color': palette.background,
    'margin': 0
}
    
def init_widgets_list():
    return widgets_list

def init_screens():
	return [
        Screen(top=bar.Bar(widgets=init_widgets_list(), **bar_defaults)),
    ]

screens = init_screens()
