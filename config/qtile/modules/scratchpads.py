from libqtile.config import ScratchPad, DropDown, EzKey as Key
from libqtile.lazy import lazy

from modules.prefs import prefs
from modules.groups import groups

# Scratchpads
groups.append( 
    ScratchPad("pads",[
        DropDown("top", prefs.terminal + " -e btm", x=0.15, y=0.2, width=0.70, height=0.65, on_focus_lost_hide=True),
        DropDown("mus", prefs.terminal + " -e ncmpcpp", x=0.3, y=0.4, width=0.4, height=0.2, on_focus_lost_hide=True),

    ]) 
)

