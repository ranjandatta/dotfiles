import pytz
from qtile_extras import widget

class Clock(widget.Clock):
    def __init__(self, **config):
        super().__init__(**config)
        self.toggled = False
        self.localtz = self.timezone
        self.localfmt = self.format
        self.add_callbacks({"Button1": self.toggle})

    def toggle(self):
        if self.toggled:
            self.format = self.localfmt
            self.timezone = self.localtz
        else:
            self.format = self.other_format
            self.timezone = pytz.timezone(self.other_timezone)
        self.toggled = not self.toggled
        self.update(self.poll())