from libqtile.config import EzKey as Key
from libqtile.lazy import lazy

from modules.groups import groups
from modules.prefs import prefs

keys = [
    # Switch between windows
    Key('M-<Left>',                 lazy.layout.left(),                             desc='Move focus to left'),
    Key('M-<Right>',                lazy.layout.right(),                            desc='Move focus to right'),
    Key('M-<Up>',                   lazy.layout.up(),                               desc='Move focus to up'),
    Key('M-<Down>',                 lazy.layout.down(),                             desc='Move focus to down'),
    Key('M-<Space>',                lazy.layout.next(),                             desc='Move window focus to other window'),

    #Move windows between left/right columns or move up/down in current stack.
    Key('M-S-<Left>',               lazy.layout.shuffle_left(),                     desc='Move window to left'),
    Key('M-S-<Right>',              lazy.layout.shuffle_right(),                    desc='Move window to right'),
    Key('M-S-<Up>',                 lazy.layout.shuffle_up(),                       desc='Move window to up'),
    Key('M-S-<Down>',               lazy.layout.shuffle_down(),                     desc='Move window to down'),
    
    # Grow windows.
    Key('M-C-<Left>',               lazy.layout.grow_left(),                        desc='Grow window to left'),
    Key('M-C-<Right>',              lazy.layout.grow_right(),                       desc='Grow window to right'),
    Key('M-C-<Up>',                 lazy.layout.grow_up(),                          desc='Grow window to up'),
    Key('M-C-<Down>',               lazy.layout.grow_down(),                        desc='Grow window to down'),
    Key('M-n',                      lazy.layout.normalize(),                        desc='Reset all window sizes'),

    # Toggle between different layouts 
    Key('M-<Tab>',                  lazy.next_layout(),                             desc='Toggle between layouts'),

    # Toggle bar
    Key('M-S-b',                    lazy.hide_show_bar(),                           desc='Toggle bar'),

    # Move Between Workspaces
    Key('M-S-<Tab>',                lazy.screen.next_group(),                       desc='Move to next Workspace'),
    Key('M-C-<Tab>',                lazy.screen.prev_group(),                       desc='Move to previous Workspace'),

    # Switch Monitors
    Key('M-<Period>',               lazy.next_screen(),                             desc='Next monitor'),

    # More Window Stuff
    Key('M-f',                      lazy.window.toggle_floating(),                  desc='Toggle floating window'),
    Key('M-m',                      lazy.window.toggle_fullscreen(),                desc='Toggle fullscreen'),

    # Base Qtile
    Key('M-S-r',                    lazy.restart(),                                 desc='Restart Qtile'),
    Key('M-S-q',                    lazy.shutdown(),                                desc='Shutdown Qtile'),
    Key('M-r',                      lazy.spawncmd(),                                desc='Spawn a command using a prompt widget'),
    Key('M-x',                      lazy.window.kill(),                             desc='Kill focused window'),

    #Rofi
    Key('M-p',                      lazy.spawn(prefs.launcher),                     desc='Launch Menu'),
    Key('M-q',                      lazy.spawn(prefs.power_menu),                   desc='Launch Power Menu'),
    Key('M-<Return>',               lazy.spawn(prefs.terminal),                     desc='Launch Terminal'),

    # Launch Applications
    Key('M-C-b',                    lazy.spawn(prefs.browser),                      desc='Launch Browser'),
    Key('M-C-p',                    lazy.spawn(prefs.private),                      desc='Launch Incognito Browser'),
    Key('M-C-e',                    lazy.spawn(prefs.code_editor),                  desc='Launch Editor'),
    Key('M-C-f',                    lazy.spawn(prefs.file_manager),                 desc='Launch File Manager'),
    Key('M-C-v',                    lazy.spawn('pavucontrol'),                      desc='Launch Volume Control'),
        
    # Take Screenshot
    Key('<Print>',                  lazy.spawn(prefs.screenshot_tool),              desc='Take a Screenshot'),
    Key('C-l',                      lazy.spawn('slock'),                            desc='Lock Screen'),

    # Media hotkeys
    Key('<XF86AudioRaiseVolume>',   lazy.spawn('pactl set-sink-volume 0 +5%'),      desc='Raise Volume'),
    Key('<XF86AudioLowerVolume>',   lazy.spawn('pactl set-sink-volume 0 -5%'),      desc='Lower Volume'),
    Key('<XF86AudioMute>',          lazy.spawn('pactl set-sink-mute 0 toggle'),     desc='Mute Volume'),
    Key('<XF86AudioPlay>',          lazy.spawn('playerctl play-pause'),             desc='Play / Pause Media'),
    Key('<XF86AudioNext>',          lazy.spawn('playerctl next'),                   desc='Play Next'),
    Key('<XF86AudioPrev>',          lazy.spawn('playerctl previous'),               desc='Play Previous'),

    # Brigtness
    Key('<XF86MonBrightnessUp>',    lazy.spawn('brightnessctl s 10+'),              desc='Increase Brightness'),
    Key('<XF86MonBrightnessDown>',  lazy.spawn('brightnessctl s 10-'),              desc='Decrease Brightness'), 
]

for i in groups:
    keys.extend(
        [
            Key('M-'+i.name,        lazy.group[i.name].toscreen(),                  desc='Switch to group {}'.format(i.name)),
            Key('M-S-'+i.name,      lazy.window.togroup(i.name, switch_group=False),desc='Move focused window to group {}'.format(i.name)),
        ]
    )

keys.extend(
    [
        Key('M-C-t',                lazy.group['pads'].dropdown_toggle('top'),      desc='Dropdown top'),     
        Key('M-C-m',                lazy.group['pads'].dropdown_toggle('mus'),      desc='Dropdown music'),
    ]
)