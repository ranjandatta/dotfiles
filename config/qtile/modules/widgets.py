from libqtile import qtile, widget

from qtile_extras import widget
from qtile_extras.widget.decorations import BorderDecoration

from modules.palette import palette
from modules.prefs import prefs as browser

from modules._clock import Clock
from modules._weather import Weather

def border(color: str, size=2):
    return { 
        "decorations":[
            BorderDecoration(
                colour = color,
                border_width = [0, 0, size, 0],
            )
        ]
    }
    

widgets_list = [
    widget.Spacer(length=8),
    widget.CurrentLayoutIcon(
        padding = 7,
        scale = 0.45,
        use_mask = True,
        foreground = palette.magenta,
        **border(palette.magenta)
    ),
    widget.Spacer(length=8),
    widget.GroupBox(
        active =            palette.green,
        inactive =          palette.gray,
        borderwidth =       2,
        highlight_method =  "line",
        highlight_color=palette.background,
        block_highlight_text_color=palette.blue,
        this_current_screen_border=palette.blue,
        visible_groups =["1", "2", "3", "4"],
        **border(palette.gray)
    ),
    widget.Spacer(),
    # widget.WindowName(
    #     width = bar.CALCULATED,
    #     **border(palette.foreground),
    #     empty_group_string = '',
    # ),
    Weather(
        app_key='101a959b90b240a107df3b3652ea8c47',
        cityid='5303752', 
        format='{location_city}: {icon} {main_temp} °{units_temperature}',
        other_city='1271715',
        foreground=palette.green,
        metric=False,
        mouse_callbacks = {'Button3': lambda: qtile.cmd_spawn(prefs.browser + ' https://www.wunderground.com/weather/us/az/maricopa/KAZMARIC167')},
    ),
    widget.Spacer(),
    widget.WidgetBox(
        fontsize = 18,
        foreground=palette.white,
        text_closed = '',
        text_open = '',
        widgets=[
            widget.DF(
                format='  {r:.0f}%',
                visible_on_warn=False,
                foreground=palette.red,
                **border(palette.red),
                mouse_callbacks = {'Button3': lambda: qtile.cmd_spawn("xdotool key super+control+t")},
            ),
            widget.Spacer(length=8),        
            widget.Memory(
                format='󰍛  {MemPercent}%',
                foreground=palette.blue,
                **border(palette.blue),
                mouse_callbacks = {'Button3': lambda: qtile.cmd_spawn("xdotool key super+control+t")},
            ),
            widget.Spacer(length=8),        
            widget.CPU(
                format='   {load_percent}%',
                foreground=palette.green,
                **border(palette.green),
                mouse_callbacks = {'Button3': lambda: qtile.cmd_spawn("xdotool key super+control+t")},
            ),
        ]
    ),
    widget.Spacer(length=8),
    widget.PulseVolume(
        #volume_app = 'pavucontrol',
        foreground = palette.yellow,
        emoji=True,
        emoji_list=['󰝟', '󰕿', '󰖀', '󰕾'],
        fontsize=20,
        **border(palette.yellow),
        mouse_callbacks = {'Button3': lambda: qtile.cmd_spawn('xdotool key super+control+v')},
    ),
    widget.PulseVolume(
        #volume_app = 'pavucontrol',
        foreground = palette.yellow,
        padding = 0,
        **border(palette.yellow),
        mouse_callbacks = {'Button3': lambda: qtile.cmd_spawn('xdotool key super+control+v')},
    ),
    widget.Spacer(length=12, **border(palette.yellow)),
    widget.Spacer(length=8),
    Clock(
        format="󰔛  %I:%M %p",
        other_format="󱡼  %I:%M %p",
        other_timezone='Asia/Kolkata',
        foreground = palette.magenta,
        **border(palette.magenta)
    ),
    widget.Spacer(length=8),        
]



